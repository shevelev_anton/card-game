import { CardWeight } from 'interfaces/card-weight';

export const SUITS = ['spade', 'heart', 'diamond', 'club'];
export const CARDS: CardWeight[] = [
  {
    value: '2',
    weight: 2,
  },
  {
    value: '3',
    weight: 3,
  },
  {
    value: '4',
    weight: 4,
  },
  {
    value: '5',
    weight: 5,
  },
  {
    value: '6',
    weight: 6,
  },
  {
    value: '7',
    weight: 7,
  },
  {
    value: '8',
    weight: 8,
  },
  {
    value: '9',
    weight: 9,
  },
  {
    value: '10',
    weight: 10,
  },
  {
    value: 'J',
    weight: 11,
  },
  {
    value: 'Q',
    weight: 12,
  },
  {
    value: 'K',
    weight: 13,
  },
  {
    value: 'A',
    weight: 14,
  },
];
// '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'
