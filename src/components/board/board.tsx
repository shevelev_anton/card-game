import React, { Component } from 'react';
import { observer } from 'mobx-react';
import { Player } from 'interfaces/player';
import { PlayerComponent } from 'components';
import { PlayerStore } from 'storage';
import { CardService } from 'services';
import './board.scss';

@observer
export default class BoardComponent extends Component {
  componentDidMount() {
    this.playGame();
  }
  renderPlayer = (player: Player) => (
    <PlayerComponent {...{ player }} key={player.id} />
  );

  playGame = () => {
    CardService.instance.playGame();
  };
  render() {
    const { players, winner } = PlayerStore.instance;
    return (
      <div className='board'>
        {players.map(this.renderPlayer)}
        {winner ? <div>{winner.name} won</div> : <div>its a tie</div>}
        <button className='play-again' onClick={this.playGame}>
          Play again
        </button>
      </div>
    );
  }
}
