import { CardWeight } from './card-weigh';

export interface Card {
  id: number;
  suit: string; // one of ['spade', 'heart', 'diamond', 'club']
  value: CardWeight;
}
